
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
import pickle as pkl
from sklearn.externals import joblib

train_df = pkl.load(open( "./rgb_names.pkl", "rb" ))

unique_color_names = train_df['color name'].unique()
classes = dict(zip(list(range(len(unique_color_names))), unique_color_names)) 
inverse_classes = dict(zip(unique_color_names, list(range(len(unique_color_names)))))
train_df["color number"] = train_df["color name"].apply(lambda x: inverse_classes[x])

num_models = 5
knn_models = []

for i in range(1, num_models + 1):
    neigh = KNeighborsClassifier(n_neighbors=i, p=1, n_jobs=-1)
    neigh.fit(train_df[["red", "green", "blue"]], train_df["color number"])
    knn_models.append(neigh)

#joblib.dump(knn_models, "1-10_weirdness_knn_models.joblib")
#joblib.dump(knn_classes, "knn_classes.joblib")


while(1):
    red_input = int(input("red: "))
    green_input = int(input("green: "))
    blue_input = int(input("blue: "))
    weirdness = 10 - int(input("weirdness [1 to 5]: "))
    output = classes[knn_models[weirdness].predict([[red_input, green_input, blue_input]])[0]]
    print(output)

