import csv
import pandas as pd
import re
import sys
import lemonbar
print("starting")
NUM_LINES = 3408036
column_names = ["red", "green", "blue", "color name"]
bar = lemonbar.Estimator(NUM_LINES)
x=0
temp_dict = {}
with open('colors.txt', newline='\n') as csvfile:
    re_lines = re.findall('0\,\d+\,\d+\,\d+\,[^\n]*', csvfile.read())
    print("done reading")
    for line in re_lines:
        line_list = line.split(',')
        red = line_list[1]
        green = line_list[2]
        blue = line_list[3]
        color_name = "".join(line_list[4:])
        clean_row = [red, green, blue, color_name]
        #df.iloc[x] = clean_row
        temp_dict[x] = clean_row
        bar.next()
        x+=1
    df = pd.DataFrame.from_dict(temp_dict, "index", columns=column_names)
    print(df)

#df = pd.DataFrame.from_csv('./colors.txt')
#pass
df.to_pickle("./rgb_names.pkl")
bar.finish()
print("done")