from __future__ import print_function

import math

from IPython import display
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.python.data import Dataset
import pickle as pkl

pd.options.display.max_rows = 10
print("loading colors")
colors_df = pkl.load(open( "./rgb_names.pkl", "rb" ))
print("colors loaded")
unique_color_names = colors_df['color name'].unique()

classes = dict(zip(list(range(len(unique_color_names))), unique_color_names)) # convert strings to ints for tensorflow classes
inverse_classes = dict(zip(unique_color_names, list(range(len(unique_color_names)))))

# convert classes to ints
colors_df["color number"] = colors_df["color name"].apply(lambda x: inverse_classes[x])

# feature columns
red_column = tf.feature_column.numeric_column("red")
green_column = tf.feature_column.numeric_column("green")
blue_column = tf.feature_column.numeric_column("blue")
feat_columns = [red_column, green_column, blue_column]

labels = colors_df["color number"]
x_data = colors_df.drop(["color number", "color name"], axis=1).astype(int)

# t/t split
x_train, x_test, y_train, y_test = train_test_split(x_data, labels, test_size=.33, random_state=0)

# input function
input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(   
    x = x_train, 
    y = y_train,
    batch_size = 100,
    num_epochs = 1,
    shuffle = False,
)

# model training
model = tf.estimator.LinearClassifier(feature_columns=feat_columns, n_classes=len(classes))
print("training model")
model.train(input_fn=input_fn, steps=100)
print("model trained")


# model predictions
pred_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(
    x = x_test,
    batch_size = 100,
    num_epochs = 1,
    shuffle = False,
    queue_capacity = 10,
)
print("making predictions")
predictions = list(model.predict(input_fn=pred_input_fn))
print("done making predictions")
print(predictions[0])
pass

